# Sales and Distribution Network

Small Java console application which simulates production and distribution from factories to sales outlets within a company.




Assignment:


Make a simulation of production and sales network of electronic devices by respecting basic principles of object oriented programming. 

Note:
1. No date base should be used
2. No need to write html/css cod
3. Emphasis is on OOP
4. Try to implement following concepts in appropriate places:
-inheritance
-abstract classes
-polymorphism
-singeleton pattern

Assignment:

Factories keep their records on production and distribution of products in sales places. During production, each product is assigned unique serial number. Sales places can submit requests for delivering required products to the headquarter, by stating products and their amount. There are more factories and sales places within company, where sales places are sending requests to the company headquarter, and each factory may deliver products to any sales place.

Create a simulation script which does the following:

1. Create a company
2. Create factory “El Facto 1” within company
3. Create sales place “Super Tronics” within company
4. Create an employee in factory, Mark Johnson, roll: production worker
5.  Create an employee in factory, Mary Peterson, roll: production supervisor. This employee is superior to Mark Johnson
6. Create an employee in factory, Tim Green, roll: transport supervisor
7. Create an employee in factory, Mike Trenton, roll: seller
8. Create 20 monitors in factory “El Facto 1”, under supervision of Mary Peterson
9. Send request from sales place “Super Tronics” for delivering 10 monitors and 50 keyboards
10. Transport 10 monitors and 30 keyboards from factory “El Facto 1” to sales place “Super Tronics”, under supervision of Tim Green
11. Create a buyer Michael Grey
12. Michael buys a monitor from “Super Tronics” for the price of 80eur, pays in cash, from the seller Mike Trenton


Add logging of actions in the system. Actions needs to be logged in a .txt file(s) in the following format [date and time] [action] [additional information]
Possible example of one line from log file:
[20.03.2016 11:30] Transport, products: 10 x keyboard, 15 x mouse, from: El Facto 1, to: Super Tronics, supervisor Tim Green

Actions which need to be logged:
1. Production of every product individually (serial number, tip of product, factory in which product was made, date and time, name of supervisor)
2. Delivery requests (sales place which submits the request, list of products with amounts)
3. Products transport (factory name, name of destination – that is sales place, list of products with amounts, supervisor name)
4. Each sale (list of serial numbers, sales place, name of the employee who made a sale, means of payment ( cash/credit card), name of the buyer (if it is payed by credit card)