package DAO;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import javax.swing.JOptionPane;

import model.Identifiable;
import model.Product;

public interface DAOinterface {
	
	//files
	public static final String productsPath = "products.txt";
	public static final String workersPath = "workers.txt";
	public static final String sellersPath = "sellers.txt";
	public static final String elementsPath = "elements.txt";
	public static final String requestsPath = "requests.txt";
	public static final String shipmentsPath = "shipments.txt";
	public static final String shipmentProductsPath = "shipmentProducts.txt";
	public static final String outletsPath = "outlets.txt";
	public static final String factoriesPath = "factories.txt";
	public static final String companiesPath = "companies.txt";
	
	public static final String serialNumberPath = "serialNumber.txt";
	
	public static final String requestLogPath = "requestLog.txt";
	public static final String productionLogPath = "productionLog.txt";
	public static final String transportLogPath = "transportationLog.txt";
	public static final String salesLogPath = "salesLog.txt";
	
	public static final String temporaryFilePath = "temporary.txt";
	
	public static HashMap<Integer,Identifiable> ucitajSve(CreateFromStringInterface createFromString, String path) {
		
		var mapa = new HashMap<Integer,Identifiable>();
		
		BufferedReader reader = null;
		
		try {
			reader = FileUtils.getReader(path);
		
			String line = "";
			while((line = reader.readLine()) != null  ) {
				
				if(!line.equals("")) {
					var object = createFromString.CreateFromString(line);
					mapa.put(object.getId(), object);
				} else {
					continue;
				}
			}
		
			reader.close();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null,e.getMessage());
			System.out.println("PERAA");
			e.getStackTrace();
		}
		return mapa;
	}
	
	
	
	
	public static int generateSerial() {

		BufferedReader reader = null;
		BufferedWriter writer = null;
		int serialNumuber, newSerialNumber;
		try {
			reader = FileUtils.getReader(DAOinterface.serialNumberPath);
			serialNumuber = Integer.valueOf(reader.readLine());
			
			newSerialNumber = serialNumuber + 1;
			
			writer = FileUtils.getWriter(DAOinterface.serialNumberPath, false);
			writer.write(String.valueOf(newSerialNumber));
			
			reader.close();
			writer.close();
			
			return serialNumuber;
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("Debugging purposes");
		System.exit(1);
		return -1;
		
	}
	
	
	
	public static void dodajLog(LogInterface object, String path) throws IOException{
		
		
		BufferedWriter writer = null;
		try {
			writer = FileUtils.getWriter(path,  true);
			writer.append(object.writeLogToFile());
			//writer.newLine();
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
			e.printStackTrace();
			
		} finally {
			try {
				writer.close();

			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, e.getMessage());
				e.printStackTrace();
				
			}

		}
	}
	
	
	
	public static void dodaj(Identifiable object, String path) throws IOException{
		
		
		BufferedWriter writer = null;
		try {
			writer = FileUtils.getWriter(path,  true);
			writer.append(object.WriteToString());
			writer.newLine();
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
			e.printStackTrace();			
		} finally {
			try {
				writer.close();

			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, e.getMessage());
				e.printStackTrace();			
			}

		}

	}
	
	
	public static void obrisi(Identifiable object, String path) {
		

		BufferedReader reader = null;
		BufferedWriter writer = null;

		try {
			reader = FileUtils.getReader(path);
			writer = FileUtils.getWriter(temporaryFilePath, true);
			String line = "";
			while ((line = reader.readLine()) != null) {
				if (line.equals("")) {
					continue;
				}
				if (object.getId() != getIdFromString(line)) {
					writer.write(line);
					writer.newLine();
				}
			}
			
			
			reader.close();
			writer.close();
			File original = FileUtils.getFileForName(path);
			File temporary = FileUtils.getFileForName(temporaryFilePath);
			
			original.delete();
			temporary.renameTo(FileUtils.getFileForName(path));

		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
			e.printStackTrace();
		}

	}
	
	
	public static void izmeni(Identifiable object, String path) throws IOException{

		BufferedReader reader = null;
		BufferedWriter writer = null;
		
		try {
			
			reader = FileUtils.getReader(path);
			writer = FileUtils.getWriter(temporaryFilePath, true);
			
			
			String line = "";
			while ((line = reader.readLine()) != null) {
				if (line.equals("")) {
					continue;
				}
				if (object.getId() == getIdFromString(line)) {
					writer.write(object.WriteToString());
					writer.newLine();
				} else {
					writer.write(line);
					writer.newLine();
				}
			}
			
			reader.close();
			writer.close();
			
			File original = FileUtils.getFileForName(path);
			File temporary = FileUtils.getFileForName(temporaryFilePath);
			
			original.delete();
			temporary.renameTo(FileUtils.getFileForName(path));
			
			
			
			
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
			e.printStackTrace();
		} 
	
	}
	
	
	public static void dodajProduct(Product product, String path, int idShipment) throws IOException{
		BufferedWriter writer = null;
		try {
			writer = FileUtils.getWriter(path,  true);
			writer.append(product.writeProductToShipment(idShipment));
			writer.newLine();
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
			e.printStackTrace();			
		} finally {
			try {
				writer.close();

			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, e.getMessage());
				e.printStackTrace();			
			}

		}
	}

	
	
	private static int getIdFromString(String text) throws NumberFormatException {
		int id = 0;
		String[] a = text.split("\\|");
		id = Integer.parseInt(a[0]);
		return id;
	}
	

}
