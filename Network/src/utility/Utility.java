package utility;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.HashMap;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import model.Identifiable;


public class Utility {
	
	private static Scanner input = new Scanner(System.in);
	
	public static int choice(int last) {	//last je broj opcija koji je trenutno dostupan korisniku da odabere
		
		int answer;
		while (true) {
			System.out.print("Vaš izbor je: ");
			String choice = input.nextLine();
			if (isInteger(choice)) {
				answer = Integer.parseInt(choice);
				if ((answer >= 1 && answer <= last) || answer == 0) {
					break;
				} else {
					System.out.println("Unesite odgovarajucu vrednost!");
				}
			} else {
				System.out.println("Unesite odgovarajucu vrednost!");
			}
		}
		
		return answer;
	}
	
	public static int choiceForElement(int last, String poruka) {	//last je broj opcija koji je trenutno dostupan korisniku da odabere
		
		int answer;
		while (true) {
			System.out.print(poruka);
			String choice = input.nextLine();
			if (isInteger(choice)) {
				answer = Integer.parseInt(choice) - 1;
				if ((answer >= 0 && answer <= last)) {
					break;
				} else {
					System.out.println("Unesite odgovarajucu vrednost!");
				}
			} else {
				System.out.println("Unesite odgovarajucu vrednost!");
			}
		}
		
		return answer;
	}
	
	public static Date createDate(String datum) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		Date date = null;
		try {  
			//String dateString = "19-06-2017";
			//String to date conversion
			date = sdf.parse(datum);  
       
		} catch (Exception e) {
			System.out.println("Greska pri konverziji datuma");
			//e.printStackTrace();
			//System.exit(1);
			return null;
		}
		return date;   
		}
	
	public static Date createDateWithTime(String datum) {
		DateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
		Date date = null;
	    try {  
	    	
	       date = sdf.parse(datum);  
	       
	    } catch (Exception e) {
	    	 System.out.println("Greska pri konverziji stringa datuma i vremena u Date");
		     e.printStackTrace();
		     System.exit(1);
		}
	    return date;
	}
	
	
	public static boolean isInteger(String s){
		try {
			Integer.parseInt(s);
		} catch (Exception e) {
			return false;
		}
		return true;
	}
	
	public static String convertDateToString(Date datumIsteka) {
		String datum = null;
		String pattern = "dd-MM-yyyy";
		DateFormat dateFormat = new SimpleDateFormat(pattern);
		try {
			datum = dateFormat.format(datumIsteka);
		} catch (Exception e ) {
			 System.out.println("Greška pri konverzija Date-a u String");
			 e.printStackTrace();
		}
		return datum;
	}
	
	public static String convertDateWithTimeToString(Date d) {
		String datum = null;
		String pattern = "dd-MM-yyyy HH:mm";
		DateFormat dateFormat = new SimpleDateFormat(pattern);
		try {
			datum = dateFormat.format(d);
		} catch (Exception e ) {
			 System.out.println("Greska pri konverzija Date-a u String");
			 e.printStackTrace();
		}
		return datum;
	}
	
	public static Date unesiDatum(String poruka) {
		//*Time pattern format is set to be dd-mm-yyyy HH:mm*//
		Pattern pat = Pattern.compile("^^(3[01]|[12][0-9]|0[1-9])-(1[0-2]|0[1-9])-[0-9]{4} (0[0-9]|[12][0-9]):([0-5][0-9])$");
		
		while (true) {
			String datum = ocitajTekst(poruka);
			Matcher matcher = pat.matcher(datum);
			if (matcher.matches() && isValid(datum)) {
				Date date = createDateWithTime(datum);
				return date;
			} else {
				System.out.println("Pogrešno unet format datuma! Pokušajte ponovo");
			}
		}
	}
	

	
	public static Date threeYearsFromNow(int yearsFromNow) {
		LocalDateTime now =  LocalDateTime.now();
		LocalDateTime threeYearsFromNow= now.plusYears(yearsFromNow);
		return Date.from(threeYearsFromNow.atZone(ZoneId.systemDefault()).toInstant());
	}
	
	
	public static boolean isValid(String datum) {
		
		if(datum == null){
			return false;
		}
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
		sdf.setLenient(false);
		try {
			//ako nije validan vraca false
			@SuppressWarnings("unused")
			Date date = sdf.parse(datum);
			
		} catch (ParseException e) {
			return false;
		}
		return true;
	}
	
	//citanje promenljive String
	public static String ocitajTekst(String poruka){
		String tekst = "";
		while(tekst == null || tekst.equals("")) {
			System.out.print(poruka);
			tekst = input.nextLine();
		}
		
		return tekst;
	}
	
	public static float ocitajRealanBroj(String poruka){
		System.out.print(poruka);
		while (input.hasNextFloat()==false) {
			System.out.println("GRESKA - Pogresno unsesena vrednost, pokusajte ponovo: ");
			input.nextLine();
		}
		
		float realanBroj = input.nextFloat();
		input.nextLine(); //cisti sve sa ulaza sto nije broj ili ostatak teksta posla broja
		return realanBroj;
	}
	
	public static char ocitajKarakter(){
		char karakter = ' ';
		boolean notRead = true;
		do {
			String text = input.next();
			if(text.length()==1){
				karakter = text.charAt(0);
				notRead = false;
			}
			else {
				System.out.println("GRESKA - Pogresno unsesena vrednost za karakter, pokusajte ponovo: ");
			}
			input.nextLine();//cisti sve sa ulaza sto nije broj ili ostatak testa posle karaktera
		} while (notRead);
		
		
		
		return karakter;
	}
	
	//citanje promenljive char
	public static char ocitajOdlukuOPotvrdi(String tekst){
		System.out.println("Da li zelite " + tekst + " [Y/N]:");
		char odluka = ' ';
		while( !(odluka == 'Y' || odluka == 'N') ){
			odluka = ocitajKarakter();
			if (!(odluka == 'Y' || odluka == 'N')) {
				System.out.println("Opcije su Y ili N");
			}
		}
		return odluka;
	}
	
	public static int ocitajCeoBroj(String poruka){
		System.out.print(poruka);
		while (input.hasNextInt()==false) {
			System.out.println("GRESKA - Pogresno unsesena vrednost, pokusajte ponovo: ");
			input.nextLine();
		}
		
		int ceoBroj = input.nextInt();
		input.nextLine(); //cisti sve sa ulaza sto nije broj ili ostatak teksta posla broja
		return ceoBroj;
	}
	
	
	public static int ocitajId(HashMap<Integer, Identifiable> mapa, String poruka) {
		
		int id; 
		
		do {
			id = Utility.ocitajCeoBroj(poruka);
			if (!mapa.containsKey(id)) System.out.println("Ne postoji id pod unešenim brojem. Pokušajte ponovo.");
		} while (!mapa.containsKey(id));
		
		return id;
	}
	
	public static int ocitajSentinelBroj(String poruka, int limit, int limit2) {
		
		do {
			int broj = ocitajCeoBroj(poruka);
			if (broj <= limit && broj > 0 && broj <= limit2) return broj;
			System.out.println("Unesite odgovarajucu vrednost!");
		} while(true);
		
	}
	
	
	
	
	
} //od klase
