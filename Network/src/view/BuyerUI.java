package view;

import java.util.ArrayList;
import java.util.Date;

import controller.DataStore;
import display.Display;
import model.Company;
import model.Element;
import model.Outlet;
import model.Product;
import model.Request;
import model.SalesLog;
import model.Shipment;
import utility.Utility;

public class BuyerUI {
	
	public static void buyerStartMenu() {
		
		while(true) {
			System.out.println("1. Napravi porudzbinu\n2. Proveri da li je porudzbina stigla i plati ako jeste\n3. Povrtak na prethodni meni\n0. Izlazak iz aplikacije");
		
			int izbor = Utility.choice(3);
			
			switch(izbor) {
				case 1:
					makeOrder();
					break;
				case 2:
					checkForOrder();
					break;
				case 3:
					return;
				case 0:
					System.exit(1);
					
			}
		
		}
		
	}
	
	
	public static void makeOrder() {
		int idCompany = DataStore.companies.get(0).getId();
		Company company = (Company) DataStore.companies.get(idCompany);
		
		Outlet choosenOutlet = Display.chooseOutlet(company, "Izaberite prodajno mesto iz kojeg zelite da kupite proizvode");
		
		Request buyersRequest = new Request();
		
		buyersRequest.setId(DataStore.generateId(DataStore.requests));
		buyersRequest.setOutlet(choosenOutlet.getId());
		buyersRequest.setSeller(-1); // -1 means, Seller to be determined by Outlet
		buyersRequest.setFactory(-2); // -2 means, this Request is routed to Outlet, not to Company or Factory
		buyersRequest.setElements(assigneNewElementsForRequest(choosenOutlet, buyersRequest.getId()));
		buyersRequest.setDate(new Date());
		
		
		try {
			DataStore.dodaj(buyersRequest);
			choosenOutlet.getRequests().add(buyersRequest); //dodajemo zahtev onom outletu za koga je namenjeno
			DataStore.requests.put(buyersRequest.getId(), buyersRequest);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		System.out.println("Uspe�no poslata porudzbina. ID Va�e porudzbine je: " + buyersRequest.getId());
		System.out.println("Molimo Vas zapamtite ovaj broj");
		
		
	} //od metode
	
	
	public static ArrayList<Element> assigneNewElementsForRequest(Outlet choosenOutlet, int idRequest){
		
		ArrayList<Element> elementsForRequest = new ArrayList<Element>();
		
		Display.displayProductsFromOutlet(choosenOutlet);
		
		while(true) {
			System.out.println("1. Izaberi novi proizvod sa kolicinom\n0. Potvrdi porudzbinu");
			
			int izbor = Utility.choice(1);
			
			switch(izbor) {
				case 1:
					createNewElements(choosenOutlet,elementsForRequest,idRequest);
					break;
				case 0:
					if (elementsForRequest.size() == 0) {
						System.out.println("Porudzbina mora da sadrzi najmanje jedan proizvod.");
						break;
					} else return elementsForRequest;
			}
			
		}
	
		
	}
	
	
	public static void createNewElements(Outlet choosenOutlet, ArrayList<Element> elementsForRequest, int idRequest) {
		
		
		while(true) {
			int idProduct = Utility.ocitajCeoBroj("Unesite ID proizvoda: ");
			if(!inList(idProduct, elementsForRequest)) {
				if (!inListOfOutletsProducts(idProduct, choosenOutlet)) {
					Element newElement = new Element();
					newElement.setId(DataStore.generateId(DataStore.elements));
					newElement.setProduct(idProduct);
					newElement.setAmount(Utility.ocitajCeoBroj("Unesite kolicinu proizvoda: "));
					newElement.setRequest(idRequest);
					
					try {
						DataStore.dodaj(newElement);
						elementsForRequest.add(newElement);
						DataStore.elements.put(newElement.getId(), newElement);
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					return;
				} else {
					System.out.println("Unesite odgovarajuci ID.");
					if (Utility.ocitajOdlukuOPotvrdi("nastavite sa akcijom dodavanja proizoda") == 'Y') {
						return;
					}
				}
				
			} else {
				System.out.println("Izabrani prozvod se vec nalazi na porudzbini");
				if (Utility.ocitajOdlukuOPotvrdi("nastavite sa akcijom dodavanja proizoda") == 'Y') {
					return;
				}
			}
		}
		
	}
	
	private static boolean inListOfOutletsProducts(int idProduct, Outlet choosenOutlet) {
		for (Product pr : choosenOutlet.getProducts()) {
			if (pr.getId() == idProduct) return false;
		}
		return true;
	}
	
	
	private static boolean inList(int idProduct, ArrayList<Element> elementsForRequest) {
		
		for (Element e : elementsForRequest) {
			if ( e.getProduct() == idProduct) return true;
		}
		
		return false;
	}
	
	
	
	public static void checkForOrder() {
		
		int idCompany = DataStore.companies.get(0).getId();
		Company company = (Company) DataStore.companies.get(idCompany);
		
		
		Outlet outletForShipment = Display.chooseOutlet(company, "Izaberite prodajno mesto da proverite da li je Va�a porudzbina zavr�ena.");
		
		if (outletForShipment.getShipments().size() == 0) {
			System.out.println("Izabrano prodajno mesto trenutno nema nijednu pritiglu porudzbinu.");
			return;
		}
		
		Shipment shipmentForSale = Display.chooseShipment(outletForShipment);
		
		if (shipmentForSale == null) return;
		
		Request shipmentsRequest = (Request) DataStore.requests.get(shipmentForSale.getRequest());
		
		System.out.println(shipmentsRequest.displayRequestForOutlet());
		
		System.out.println("Ukupna cena Va�e porudzbine iznosi: "  + calculateTotal(shipmentForSale));
		
		System.out.println("Izaberite nacin placanja.");
		
		System.out.println("1. Gotovina\n0. Kartica");
		
		
		
		
		
			int izbor = Utility.choice(1);
			String meansOfPaying, name;
			switch(izbor) {
				
			case 1:
				
				meansOfPaying  = "Gotovina";
				name = "";
				try {
					DataStore.dodajLog(new SalesLog(shipmentForSale, name, meansOfPaying));
					outletForShipment.getShipments().remove(shipmentForSale);
					DataStore.obrisi(shipmentsRequest);
					DataStore.obrisi(shipmentForSale);
					DataStore.requests.remove(shipmentsRequest.getId());
					DataStore.shipments.remove(shipmentForSale.getId());
				} catch (Exception e) {
					e.printStackTrace();
				}
				break;
			case 0:
			
				meansOfPaying = "Kartica";
				name = Utility.ocitajTekst("Unesite Va�e ime: ");
				try {
					DataStore.dodajLog(new SalesLog(shipmentForSale, name, meansOfPaying));
					outletForShipment.getShipments().remove(shipmentForSale);
					DataStore.obrisi(shipmentsRequest);
					DataStore.obrisi(shipmentForSale);
					DataStore.requests.remove(shipmentsRequest.getId());
					DataStore.shipments.remove(shipmentForSale.getId());
				} catch (Exception e) {
					e.printStackTrace();
				}
				break;
			
				
			}
		
		
		
		
		
		
	}
	
	
	private static double calculateTotal(Shipment shipmentForSale) {
		
		return shipmentForSale.getProducts().stream() 
			   .mapToDouble(i -> i.getPrice()).sum();
		
		
	}
	
	
	
	
	
	
	
	
	

}//OD klase
