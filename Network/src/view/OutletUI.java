package view;

import java.util.ArrayList;
import java.util.Collections;

import comparator.RequestComparator;
import controller.DataStore;
import display.Display;
import model.Company;
import model.Outlet;
import model.Request;
import model.RequestLog;
import utility.Utility;

public class OutletUI {
	
	public static void outletStartMenu() {
		
		int idCompany = DataStore.companies.get(0).getId();
		Company company = (Company) DataStore.companies.get(idCompany);
		Outlet currentOutlet = Display.chooseOutlet(company,"Izaberite prodajno mesto sa kojim zelite da radite.");
		
		
		while(true) {
			System.out.println("1. Procesiraj zahteve kompaniji\n2. Povratak u prethodni meni\n0. Izlazak iz aplikacije");
			
			int izbor = Utility.choice(2);
			
			switch (izbor) {
				case 1:
					processBuyersRequest(currentOutlet, company);
					break;
				case 2:
					return;
				case 0:
					System.exit(1);
					
			}
		}
	}
	
	
	public static void processBuyersRequest(Outlet currentOutlet, Company company) {
		
		
		if (currentOutlet.getRequests().size() == 0) {
			System.out.println("Trenutno za ovo prodajno mesto, ne postoje porudzbenice kupaca.");
			return;
		}
		
		ArrayList<Request> requestsList = currentOutlet.getRequests();
		
		
		Request choosenRequest = chooseRequest(requestsList);
		
		choosenRequest.setOutlet(currentOutlet.getId());
		choosenRequest.setSeller(chooseSeller(currentOutlet));
		choosenRequest.setFactory(-1); // -1 means, this Request is going to be routed to this outlet's company
		
		
		try {
			DataStore.izmeni(choosenRequest);
			currentOutlet.getRequests().remove(choosenRequest); //uklanjamo ovaj zahtev iz liste zahteva jer je upravo procesuiran
			company.getRequests().add(choosenRequest); //slanje kompaniji zahtev
			DataStore.dodajLog(new RequestLog(choosenRequest)); //logovanje akcije
		} catch (Exception e) {
			// TODO: handle exception
		}
		System.out.println("Zahtev uspe�no proseldjen kompaniji.");
		
	}
	
	public static Request chooseRequest(ArrayList<Request> requestsList) {
		
		System.out.println("Izaberite nacin prikaza zahteva kupaca");
		while (true) {
			System.out.println("1. Od najstarijeg ka najnovijem zahtevu\n2. Od najnovijeg ka najstarijem zahtevu");
			int sorter = Utility.choice(2);
			int indexRequest;
			switch (sorter) {
				
				case 1:
					Collections.sort(requestsList, new RequestComparator(1));
					Display.displayRequestsForOutlet(requestsList);
					indexRequest = Utility.choiceForElement(requestsList.size()-1, "Unesite redni broj zahteva koji zelite da procesuirate: ");
					return requestsList.get(indexRequest);
					
				case 2:
					Collections.sort(requestsList, new RequestComparator(-1));
					Display.displayRequestsForOutlet(requestsList);
					indexRequest = Utility.choiceForElement(requestsList.size()-1, "Unesite redni broj zahteva koji zelite da procesuirate: ");
					return requestsList.get(indexRequest);
					
				default:
					System.out.println("Unesite odgovarajucu vrednost.");
					
			}
			
				
				
		}
		
	}
	
	
	public static int chooseSeller(Outlet currentOutlet) {
		
		Display.displaySellersForOutlet(currentOutlet);
		
		return Display.choosSellerFromOutlet(currentOutlet);
		
		
	}
	
	
	
} //od klase
