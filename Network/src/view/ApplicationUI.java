package view;

import controller.DataStore;
import utility.Utility;

public class ApplicationUI {

	public static void main(String[] args) {
		
		DataStore.initialLoad(); //Podizanje objekata i uspostavljanje referenci
		
		
		System.out.println("\nDobrodošli.\n");
		
		
		while (true) {
			System.out.println("1. Kompanija\n2. Prodajna mesta\n3. Fabrike\n4. Kupac\n0. Izlaz iz aplikacije");
			
			int izbor = Utility.choice(4);
			
			switch (izbor) {
				case 1: 
					CompanyUI.companyStartMenu();
					break;
				case 2:
					OutletUI.outletStartMenu();
					break;
				case 3:
					FactoryUI.factoryStartMenu();
					break;
				case 4:
					BuyerUI.buyerStartMenu();
					break;
				case 0:
					System.exit(1);
			}
		}

	}

}
