package view;

import java.util.ArrayList;
import java.util.Collections;

import DAO.DAOinterface;
import comparator.RequestComparator;
import controller.DataStore;
import display.Display;
import model.Company;
import model.Element;
import model.Factory;
import model.Outlet;
import model.Product;
import model.ProductionLog;
import model.Request;
import model.Role;
import model.Shipment;
import model.TransportationLog;
import model.Worker;
import utility.Utility;

public class FactoryUI {
	
	public static void factoryStartMenu() {
		
		int idCompany = DataStore.companies.get(0).getId();
		Company company = (Company) DataStore.companies.get(idCompany);
		Factory currentFactory = Display.chooseFactory(company,"Izaberite fabriku sa kojom zelite da radite.");
		
		
		while (true) {
			
			System.out.println("1. Procesuiraj zahteve kompanije\n2. Povratak u prethodni meni\n0. Izlazak iz aplikacije");
			
			int izbor = Utility.choice(2);
			switch(izbor) {
				case 1:
					processRequest(currentFactory);
					break;
				case 2:
					return;
				case 0:
					System.exit(1);
			}
			
			
		}
		
		
	}
	
	public static void processRequest(Factory currentFactory) {
		
		if (currentFactory.getRequests().size() == 0) {
			System.out.println("Trenutno za ovu fabriku, ne postoje porudzbenice kompanije.");
			return;
		}
		
		if (!haveRequiredWorkers(currentFactory)) {
			System.out.println("Trenutno ova fabrika ne moze da procesuira bilo koji zahtev kompanije zbog nedostatka odgovarajucih tipova radnika.\nFabrika mora da ima po najmanje jednog proizvodnog radnika, jednog nadzornika za proizvodnji u jednog nadzornika za transport.\nMolimo Vas obratite se kompaniji.");
			return;
					
		}
		
		Request choosenRequest =  chooseRequest(currentFactory.getRequests());
		
		Outlet outletFromRequest = (Outlet) DataStore.outlets.get(choosenRequest.getOutlet());
		
		Shipment newShipment = createNewShipment(currentFactory, choosenRequest);
		
		try {
			currentFactory.getRequests().remove(choosenRequest);
			DataStore.dodaj(newShipment);
			outletFromRequest.getShipments().add(newShipment);
			DataStore.dodajLog(new TransportationLog(newShipment));
			DataStore.obrisi(choosenRequest);
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("Zahtev uspe�no procesuiran i prosledjen prodajnom mestu.");
	}
	
	
	public static Shipment createNewShipment(Factory currentFactory, Request choosenRequest) {
		
		Shipment newShipment = new Shipment();
		
		
		
		newShipment.setId(DataStore.generateId(DataStore.shipments));
		newShipment.setRequest(choosenRequest.getId());
		newShipment.setOutlet(choosenRequest.getOutlet());
		newShipment.setFactory(currentFactory.getId());
		newShipment.setProductionSupervisor(findProductionSupervisorForShipment(currentFactory));
		newShipment.setTransportationSupervisor(findTransportationSupervisorForShipment(currentFactory));
		newShipment.setProducts(createProductsForShipment(choosenRequest,currentFactory ,newShipment.getId(), newShipment.getProductionSupervisor()));
		
		return newShipment;
		
	}
	
	public static ArrayList<Product> createProductsForShipment(Request choosenRequest, Factory currentFactory, int idShipment,int idProductionSupervisor){
		
		ArrayList<Product> productsForShipment = new ArrayList<Product>();
		
		for (Element el : choosenRequest.getElements()) {
			for (int i = 0; i < el.getAmount(); i++) {
				Product productFromElement = (Product) DataStore.products.get(el.getProduct());
				Product newProduct = new Product(productFromElement);	
				
				
				try {
					productsForShipment.add(newProduct);
					DAOinterface.dodajProduct(newProduct, DAOinterface.shipmentProductsPath, idShipment);
					DataStore.dodajLog(new ProductionLog(newProduct, idProductionSupervisor));
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
		}
		return productsForShipment;
		
	
		
	}
	
	
	
	public static int findProductionSupervisorForShipment(Factory currentFactory) {
		
		
		ArrayList<Worker> productionSupervisors = findSupervisors(currentFactory);
		
		return Display.chooseProductionSupervisorFromFactory(productionSupervisors);
	}
	
	
	
	public static ArrayList<Worker> findSupervisors(Factory currentFactory) {
		ArrayList<Worker> productionSupervisors  = new ArrayList<Worker>();
		for (Worker w : currentFactory.getWorkers()) {
			if(w.getRole() == Role.PRODUCTION_SUPERVISOR) productionSupervisors.add(w);
		}
		return productionSupervisors;
	}
	
	
	
	
	
	
	public static int findTransportationSupervisorForShipment(Factory currentFactory) {
		
		ArrayList<Worker> transporationSupervisors = findTransporters(currentFactory);
		
		return Display.chooseTransportSupervisorFromFactory(transporationSupervisors);
		
		
	}
	
	
	public static ArrayList<Worker> findTransporters(Factory currentFactory) {
		ArrayList<Worker> transporationSupervisors  = new ArrayList<Worker>();
		for (Worker w : currentFactory.getWorkers()) {
			if(w.getRole() == Role.TRANSPORT_SUPERVISOR) transporationSupervisors.add(w);
		}
		return transporationSupervisors;
	}
	
	
	public static boolean haveRequiredWorkers(Factory currentFactory) {
		int types = 3; //three worker types required: worker, supervisor, transporter
		int currentRoles = 0;
		for (Worker w : currentFactory.getWorkers()) {
			Role workerRole = w.getRole();
			if (workerRole == Role.PRODUCTION_WORKER || workerRole == Role.PRODUCTION_SUPERVISOR || workerRole == Role.TRANSPORT_SUPERVISOR) {
				currentRoles++;
				if (currentRoles == types) return true;
			}
		}
		return false;
	}
	
	public static Request chooseRequest(ArrayList<Request> requestsList) {
		
		System.out.println("Izaberite nacin prikaza porudzbenica kompanije.");
		while (true) {
			System.out.println("1. Od najstarije ka najnovijoj porudzbenici\n2. Od najnovije ka najstarijoj porudzbenici");
			int sorter = Utility.choice(2);
			int indexRequest;
			switch (sorter) {
				
				case 1:
					Collections.sort(requestsList, new RequestComparator(1));
					Display.displayRequestsForCompany(requestsList);
					indexRequest = Utility.choiceForElement(requestsList.size()-1, "Unesite redni broj zahteva koji zelite da procesuirate: ");
					return requestsList.get(indexRequest);
					
				case 2:
					Collections.sort(requestsList, new RequestComparator(-1));
					Display.displayRequestsForCompany(requestsList);
					indexRequest = Utility.choiceForElement(requestsList.size()-1, "Unesite redni broj zahteva koji zelite da procesuirate: ");
					return requestsList.get(indexRequest);
					
				default:
					System.out.println("Unesite odgovarajucu vrednost.");
					
			}
			
				
				
		}
		
	}
	
} //od klase
