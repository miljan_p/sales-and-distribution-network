package view;

import java.util.ArrayList;
import java.util.Collections;

import comparator.RequestComparator;
import controller.DataStore;
import display.Display;
import model.Company;
import model.Factory;
import model.Outlet;
import model.Product;
import model.Request;
import model.Role;
import model.Seller;
import model.Shipment;
import model.Worker;
import utility.Utility;

public class CompanyUI {
	
	public static void companyStartMenu() {
		
		while (true) {
			System.out.println("1. Napravi novu fabriku\n2. Napravi novo prodajno mesto\n3. Napravi novog radnika za fabriku\n4. Napravi prodavca za prodajno mesto\n5. Napravi novi proizvod i dodaj ga u fabriku\n6. Pregled fabrika\n7. Pregled prodajnih mesta\n8. Procesuiraj zahteve prodajnih mesta i prosledi ih odgovarajucoj fabrici\n9. Povratak na prethodni meni\n0. Izlazak iz aplikacije");
			
			int izbor = Utility.choice(9);
			
			switch (izbor) {
				case 1:
					createFactory();
					break;
				case 2:
					createOutlet();
					break;
				case 3:
					createNewWorkerForFactory();
					break;
				case 4: 
					createNewSellerForOutlet();
					break;
				case 5:
					createNewProductAndAddItToFactory();
					break;
				case 6:
					Display.factoriesOverview();
					break;
				case 7:
					Display.outletsOverview();
					break;
				case 8:
					routeRequests();
					break;
				case 9:
					return;
				case 0: 
					System.exit(1);
			}
		}
		
	}
	
	//1.
	public static void createFactory() {
		
		int idCompany = DataStore.companies.get(0).getId();
		Company company = (Company) DataStore.companies.get(idCompany);
		
		Factory newFactory = new Factory();
		
		newFactory.setId(DataStore.generateId(DataStore.factories));
		newFactory.setName(Utility.ocitajTekst("Unesite ime fabrike: "));
		newFactory.setCompany(DataStore.companies.get(0).getId());
		newFactory.setRequests(new ArrayList<Request>());
		newFactory.setWorkers(addWorkers(newFactory.getId(), company));
		newFactory.setProducts(addProductsForFactory(idCompany, newFactory.getId()));
		
		try {
			DataStore.dodaj(newFactory);
			DataStore.factories.put(newFactory.getId(), newFactory);
			company.getFactories().add(newFactory);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		System.out.println("Uspe�no ste kreirali fabriku: " + newFactory.getName());
	}
	
	public static ArrayList<Product> addProductsForFactory(int idCompany, int idFactory) {
		
		ArrayList<Product> productsForFactory = new ArrayList<Product>();
		
		while(true) {
			System.out.println("1. Dodajte postojeci proizvod iz asortimana kompanije u fabriku\n2. Kreirajte novi proizvod u asortimanu kompanije i dodajete ga u fabriku\n0. Povratak u prethodni meni");
			int izbor = Utility.choice(2);
			
			switch(izbor) {
				case 1:
					addExistingProduct(productsForFactory, idCompany);
					break;
				case 2:
					createNewPoductForCompanyAndFactory(productsForFactory, idCompany, idFactory);
					break;
				case 0:
					if (productsForFactory.size() == 0) {
						System.out.println("Morate dodati najmanje jedan proizvod u Va�u novu fabriku");
						break;
					} else return productsForFactory;
			}
		}
		
	}
	
	public static void createNewPoductForCompanyAndFactory(ArrayList<Product> productsForFactory, int idCompany, int idFactory) {
		
		var newProduct = new Product();
		
		newProduct.setId(DataStore.generateId(DataStore.products));
		newProduct.setCompany(idCompany);
		newProduct.setFactory(idFactory);
		newProduct.setName(Utility.ocitajTekst("Unesite ime/opis proizvod: "));
		newProduct.setPrice(Utility.ocitajRealanBroj("Unesite cenu proizvoda: "));
		
		productsForFactory.add(newProduct); //dodavanje proizvoda u fabriku
		Company company = (Company) DataStore.companies.get(idCompany);
		company.getProducts().add(newProduct); //dodavanje proizvoda u kompaniju
		System.out.println("Uspe�no ste dodali novi proizvod u kompanijski asortiman i u asortiman nove fabrike.");
		try {
			DataStore.dodaj(newProduct); //dodavanje proizvoda u fajl
			DataStore.products.put(newProduct.getId(), newProduct); //Dodavanje proizvoda u global mapu
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	
	
	public static void addExistingProduct(ArrayList<Product> productsForFactory, int idCompany) {
		Company company = (Company) DataStore.companies.get(idCompany);
		if (company.getProducts().size() == 0) {
			System.out.println("Trenutno izabrana kompanija ne sadrzi nijedan proizvod. Morate kreirati proizvod ili dodati novi proizvod u kompaniju!");
		} else {
			//prikaz i  izbor proizvoda
			System.out.println("Izberite ID proizvoda koji zelite da nova fabrika proizvodi.");
			Display.showCompaniesProducts(idCompany);
			Display.chooseProduct(company.getProducts(), productsForFactory);
			return;
			
		}
		
	}
	
	public static ArrayList<Worker> addWorkers(int idFactory, Company company){
		ArrayList<Worker> workers = new ArrayList<Worker>();
		
		while (true) {
			System.out.println("Izaberite tip radnika koji zelite da dodate u novu fabrku.");
			System.out.println("1. Radnik u proizvodnji\n2. Nadzornik za proizvodnju\n3. Nadzornik za transport\n0. Povratak u prethodni meni");
			
			int izbor = Utility.choice(3);
			
			switch (izbor) {
				case 1:
					workers.add(createWorker(Role.PRODUCTION_WORKER, idFactory, company));
					break;
				case 2:
					workers.add(createWorker(Role.PRODUCTION_SUPERVISOR, idFactory, company));
					break;
				case 3:
					workers.add(createWorker(Role.TRANSPORT_SUPERVISOR, idFactory, company));
					break;
				case 0:
					if (workers.size() == 0) {
						System.out.println("Morate dodati najmanje jednog radnika u Va�u novu fabriku.");
						break;
					} else return workers;
			}
		}
	}
	
	
	public static Worker createWorker(Role role, int idFactory, Company company) {
		Worker newWorker = new Worker();
		
		newWorker.setId(DataStore.generateId(DataStore.workers));
		newWorker.setRole(role);
		newWorker.setName(Utility.ocitajTekst("Ime radnika: "));
		newWorker.setSalary(Utility.ocitajRealanBroj("Unesite platu radnik: "));
		newWorker.setMobile(Utility.ocitajTekst("Unesite mobilni: "));
		newWorker.setUsername(Utility.ocitajTekst("Unesite korisnicko ime: "));
		newWorker.setPassword(Utility.ocitajTekst("Unesite lozinku: "));
		newWorker.setFactory(idFactory);
		
		try {
			DataStore.workers.put(newWorker.getId(), newWorker); //dodajemo ga u global mapu
			DataStore.dodaj(newWorker); //dodajemo ga u fajl
			company.getEmployees().add(newWorker); //dodajemo ga u kompaniju
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		return newWorker;
		
	}
	
	
	//2.
	public static void createOutlet() {
		int idCompany = DataStore.companies.get(0).getId();
		Company company = (Company) DataStore.companies.get(idCompany);
		if (company.getProducts().size() == 0) {
			System.out.println("Da bi kreirali prodajno mesto kompanija mora da ima najmanje jedan proizvod u svom asortimanu.");
			return;
		}
		
		var newOutlet = new Outlet();
		
		newOutlet.setId(DataStore.generateId(DataStore.outlets));
		newOutlet.setName(Utility.ocitajTekst("Unesite ime prodajnog mesta: "));
		newOutlet.setCompany(idCompany);
		newOutlet.setProducts(company.getProducts());
		newOutlet.setSellers(addSellers(idCompany, newOutlet.getId()));
		newOutlet.setShipments(new ArrayList<Shipment>());
		newOutlet.setRequests(new ArrayList<Request>());
		
		try {
			DataStore.dodaj(newOutlet);
			DataStore.outlets.put(newOutlet.getId(), newOutlet);
			company.getOutlets().add(newOutlet);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		System.out.println("Uspe�no dodato prodajno mesto " + newOutlet.getName() + " u kompaniju " + company.getName());
	}
	
	
	public static ArrayList<Seller> addSellers(int idCompany, int idOutlet){
		
		ArrayList<Seller> sellers = new ArrayList<Seller>();
		
		while(true) {
			System.out.println("1. Dodajte novog prodavca\n0. Povratak u prethodni meni");
			int izbor = Utility.choice(1);
			switch(izbor) {
				case 1:
					Seller newSeller = new Seller();
					
					newSeller.setId(DataStore.generateId(DataStore.sellers));
					newSeller.setRole(Role.SELLER);
					newSeller.setName(Utility.ocitajTekst("Ime prodavca: "));
					newSeller.setSalary(Utility.ocitajRealanBroj("Unesite platu prodavca: "));
					newSeller.setMobile(Utility.ocitajTekst("Unesite mobilni: "));
					newSeller.setUsername(Utility.ocitajTekst("Unesite korisnicko ime: "));
					newSeller.setPassword(Utility.ocitajTekst("Unesite lozinku: "));
					newSeller.setOutlet(idOutlet);
					
					
					
					try {
						DataStore.dodaj(newSeller); //dodavanje u fajl
						DataStore.sellers.put(newSeller.getId(), newSeller); //dodavanje u global mapu
						sellers.add(newSeller); //dodvanje u listu sellersa za outlet
						Company company = (Company) DataStore.companies.get(idCompany);
						company.getEmployees().add(newSeller); //dodavanje prodavca u kompaniju
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					System.out.println("Uspe�no dodata prodavac " + newSeller.getName() + " novo u prodajno mesto.");
					
					break;
				case 0:
					if (sellers.size() == 0) {
						System.out.println("Morate dodate najmanjeg jednog prodavca u prodajno mesto.");
						break;
					} else return sellers;
			}
		}
		
	
	}
	
	
	//3.
	public static void createNewWorkerForFactory() {
		int idCompany = DataStore.companies.get(0).getId();
		Company company = (Company) DataStore.companies.get(idCompany);
		
		if (company.getFactories().size() == 0) {
			System.out.println("Trenutno Va�a kompanija nema nijednu fabriku. Morate prvo kreirati fabriku");
			return;
		}
		
		Factory choosenFactory = Display.chooseFactory(company, "Izaberite fabriku za novog radnika.");
		
		Worker newWorker = new Worker();
		
		newWorker.setId(DataStore.generateId(DataStore.workers));
		newWorker.setRole(pickRole());
		newWorker.setName(Utility.ocitajTekst("Ime radnika: "));
		newWorker.setSalary(Utility.ocitajRealanBroj("Unesite platu radnik: "));
		newWorker.setMobile(Utility.ocitajTekst("Unesite mobilni: "));
		newWorker.setUsername(Utility.ocitajTekst("Unesite korisnicko ime: "));
		newWorker.setPassword(Utility.ocitajTekst("Unesite lozinku: "));
		newWorker.setFactory(choosenFactory.getId());
		
		try {
			DataStore.dodaj(newWorker);
			DataStore.workers.put(newWorker.getId(), newWorker);
			choosenFactory.getWorkers().add(newWorker);
			company.getEmployees().add(newWorker);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		System.out.println("Uspe�no dodat radnik " + newWorker.getName() + " u fabriku " + choosenFactory.getName());
		
	}
	
	
	public static Role pickRole() {
		System.out.println("Izaberi ulogu radnika.");
		while(true) {
			System.out.println("1. Radnik u proizvodnji\n2. Nadzornik za proizvodnju\n3. Nadzornik za transport");
			
			int izbor = Utility.ocitajCeoBroj("Va� izbor je: ");
			
			switch(izbor) {
				case 1:
					return Role.PRODUCTION_WORKER;
				case 2:
					return Role.PRODUCTION_SUPERVISOR;
				case 3: 
					return Role.TRANSPORT_SUPERVISOR;
				default:
					System.out.println("Unesite odgovrajuci izbor.");
					
			}
		
		}
	}
	
	
	//4.
	public static void createNewSellerForOutlet() {
		int idCompany = DataStore.companies.get(0).getId();
		Company company = (Company) DataStore.companies.get(idCompany);
		
		if (company.getOutlets().size() == 0) {
			System.out.println("Trenutno Va�a kompanija nema nijedno prodajno mesto. Morate prvo kreirati prodajna mesta.");
			return;
		}
		
		Outlet choosenOutlet = Display.chooseOutlet(company, "Izaberite prodajno mesto za novog prodavca.");
		
		Seller newSeller = new Seller();
		
		newSeller.setId(DataStore.generateId(DataStore.sellers));
		newSeller.setRole(Role.SELLER);
		newSeller.setName(Utility.ocitajTekst("Ime prodavca: "));
		newSeller.setSalary(Utility.ocitajRealanBroj("Unesite platu prodavca: "));
		newSeller.setMobile(Utility.ocitajTekst("Unesite mobilni: "));
		newSeller.setUsername(Utility.ocitajTekst("Unesite korisnicko ime: "));
		newSeller.setPassword(Utility.ocitajTekst("Unesite lozinku: "));
		newSeller.setOutlet(choosenOutlet.getId());
		
		try {
			company.getEmployees().add(newSeller);
			choosenOutlet.getSellers().add(newSeller);
			DataStore.sellers.put(newSeller.getId(), newSeller);
			DataStore.dodaj(newSeller);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("Uspe�no dodat proadavac " + newSeller.getName() + " u prodajno mesto " + choosenOutlet.getName());
	}
	
	
	//5.
	public static void createNewProductAndAddItToFactory() {
		int idCompany = DataStore.companies.get(0).getId();
		Company company = (Company) DataStore.companies.get(idCompany);
		
		if (company.getFactories().size() == 0) {
			System.out.println("Trenutno Va�a kompanija nema nijednu fabriku. Morate prvo kreirati fabriku");
			return;
		}
		
		Factory choosenFactory = Display.chooseFactory(company, "Izaberite fabriku gde ce se proizvoditi novi proizvod");
		
		var newProduct = new Product();
		
		newProduct.setId(DataStore.generateId(DataStore.products));
		newProduct.setCompany(idCompany);
		newProduct.setFactory(choosenFactory.getId());
		newProduct.setName(Utility.ocitajTekst("Unesite ime/opis proizvod: "));
		newProduct.setPrice(Utility.ocitajRealanBroj("Unesite cenu proizvoda: "));
		
		try {
			choosenFactory.getProducts().add(newProduct);
			company.getProducts().add(newProduct);
			DataStore.products.put(newProduct.getId(), newProduct);
			DataStore.dodaj(newProduct);
			addProductToAllOutlets(newProduct, company);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	private static void addProductToAllOutlets(Product newProduct, Company company) {
		for (Outlet o : company.getOutlets()) {
			o.getProducts().add(newProduct);
		}
	}
	
	public static void routeRequests() {
		
		int idCompany = DataStore.companies.get(0).getId();
		Company company = (Company) DataStore.companies.get(idCompany);
		
		if (company.getRequests().size() == 0) {
			System.out.println("Ne postoje pristigli zahtevi iz prodajnih mesta.");
			return;
		}
		
		Factory routingFactory = Display.chooseFactory(company, "Izaberite fabriku kojoj zelite da prosledite zahtev.");
		
		if (!Display.haveRequiredWorkers(routingFactory)) {
			System.out.println("Izabran fabrika nema potrebne radnike za procesuiranje zahteva. Molimo dodajte odgovarajuce radnike u fabriku.");
			return;
		}
		
		Request choosenRequest = chooseRequest(company.getRequests());
		choosenRequest.setFactory(routingFactory.getId());
		
		try {
			DataStore.izmeni(choosenRequest);
			company.getRequests().remove(choosenRequest);
			routingFactory.getRequests().add(choosenRequest);
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("Zahtev uspe�no prosledjen fabrici.");
	}
	
	
	
	public static Request chooseRequest(ArrayList<Request> requestsList) {
		
		System.out.println("Izaberite nacin prikaza zahteva prodajnih mesta");
		while (true) {
			System.out.println("1. Od najstarijeg ka najnovijem zahtevu\n2. Od najnovijeg ka najstarijem zahtevu");
			int sorter = Utility.choice(2);
			int indexRequest;
			switch (sorter) {
				
				case 1:
					Collections.sort(requestsList, new RequestComparator(1));
					Display.displayRequestsForCompany(requestsList);
					indexRequest = Utility.choiceForElement(requestsList.size()-1, "Unesite redni broj zahteva koji zelite da procesuirate: ");
					return requestsList.get(indexRequest);
					
				case 2:
					Collections.sort(requestsList, new RequestComparator(-1));
					Display.displayRequestsForCompany(requestsList);
					indexRequest = Utility.choiceForElement(requestsList.size()-1, "Unesite redni broj zahteva koji zelite da procesuirate: ");
					return requestsList.get(indexRequest);
					
				default:
					System.out.println("Unesite odgovarajucu vrednost.");
					
			}
		}
	}
	
	
	
	
	
} //od klase
