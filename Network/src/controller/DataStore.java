package controller;


import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import DAO.DAOinterface;
import DAO.FileUtils;
import DAO.LogInterface;
import model.Company;
import model.Element;
import model.Employee;
import model.Factory;
import model.Identifiable;
import model.Outlet;
import model.Product;
import model.ProductionLog;
import model.Request;
import model.RequestLog;
import model.SalesLog;
import model.Seller;
import model.Shipment;
import model.TransportationLog;
import model.Worker;

public class DataStore {
	
	
	
	public static HashMap<Integer, Identifiable> workers = null;
	public static HashMap<Integer, Identifiable> sellers = null;
	public static HashMap<Integer, Identifiable> products = null;
	public static HashMap<Integer, Identifiable> elements = null;
	public static HashMap<Integer, Identifiable> buyersRequests = null;
	public static HashMap<Integer, Identifiable> requests = null;
	public static HashMap<Integer, Identifiable> shipments = null;
	public static HashMap<Integer, Identifiable> outlets = null;
	public static HashMap<Integer, Identifiable> factories = null;
	public static HashMap<Integer, Identifiable> companies = new HashMap<Integer, Identifiable>();
	
	public static void initialLoad() {
		
		
		workers = DAOinterface.ucitajSve(Worker::CreateFromString, DAOinterface.workersPath);
		
		sellers = DAOinterface.ucitajSve(Seller::CreateFromString, DAOinterface.sellersPath);
		
		products = DAOinterface.ucitajSve(Product::CreateFromString, DAOinterface.productsPath);
		
		elements = DAOinterface.ucitajSve(Element::CreateFromString, DAOinterface.elementsPath);
				
		requests = DAOinterface.ucitajSve(Request::CreateFromString, DAOinterface.requestsPath);
		
		shipments = DAOinterface.ucitajSve(Shipment::CreateFromString, DAOinterface.shipmentsPath);
		
		outlets = DAOinterface.ucitajSve(Outlet::CreateFromString, DAOinterface.outletsPath);
		
		factories = DAOinterface.ucitajSve(Factory::CreateFromString, DAOinterface.factoriesPath);
		
		//companies = DAOinterface.ucitajSve(Company::CreateFromString, DAOinterface.companiesPath);
		
		companies.put(0, Company.getInstance());  //company is Singleton
	}
	
	public static int generateId(HashMap<Integer, Identifiable> list) {
		if (!list.isEmpty()) {
			return list.values().stream().map(i -> i.getId()).max(Integer::compare).get() + 1;
		} else
			return 0;
	}
	
	
	public static ArrayList<Product> findProductsForShipment(int idShipment){
		
		File shipmentProduct = FileUtils.getFileForName(DAOinterface.shipmentProductsPath);
		
		Path shipmentProductPath = shipmentProduct.toPath();
		
		//Files.lines(Path path) - lazy evaluation method - ne ucitavaju se sve linije fajla.
		//kako ih Stream "konzumira", tako se jo� linija ucita. Ucitavanje linija u memoriju se izvrsava -
		//"po potrebi".
		try(Stream<String> linesShipmentProducts = Files.lines(shipmentProductPath)) {
			ArrayList<Product> products = (ArrayList<Product>) linesShipmentProducts
					.filter(i -> !i.equals(""))
					.filter(i -> Integer.valueOf(i.split("\\|")[0]) == idShipment)
					.map(Product.mapLineToProduct).collect(Collectors.toList());
			return products;
		} catch (IOException e) {
			e.printStackTrace();
		} 

		return new ArrayList<Product>();			
				
	}
	
	public static ArrayList<Element> findElementsForRequest(int idRequest){
		return new ArrayList<Element>(elements.entrySet().stream()
				.filter(i -> ((Element) i.getValue()).getRequest() == idRequest)
				.map(i -> ((Element) i.getValue())).collect(Collectors.toList()));
	}
	
	public static ArrayList<Product> findProductsForOutlet(){
		return new ArrayList<Product>(products.entrySet().stream()
				.map(i -> ((Product) i.getValue())).collect(Collectors.toList()));
	}
	
	
	public static ArrayList<Seller> findSellersForOutlet(int idOutlet){
		return new ArrayList<Seller>(sellers.entrySet().stream()
				.filter(i -> ((Seller) i.getValue()).getOutlet() == idOutlet)
				.map(i -> ((Seller) i.getValue())).collect(Collectors.toList()));
	}
	
	public static ArrayList<Shipment> findShipmentsForOutlet(int idOutlet){
		return new ArrayList<Shipment>(shipments.entrySet().stream()
				.filter(i -> ((Shipment) i.getValue()).getOutlet() == idOutlet)
				.map(i -> ((Shipment) i.getValue())).collect(Collectors.toList()));
	}
	
	
	public static ArrayList<Request> findRequestsForOutlet(int idOutlet){
		return new ArrayList<Request>(requests.entrySet().stream()
				.filter(i -> ((Request) i.getValue()).getOutlet() == idOutlet && ((Request) i.getValue()).getFactory() == -2)
				.map(i -> ((Request) i.getValue())).collect(Collectors.toList()));
	}
	
	public static ArrayList<Product> findProductsForFactory(int idFactory){
		return new ArrayList<Product>(products.entrySet().stream()
				.filter(i -> ((Product) i.getValue()).getFactory() == idFactory)
				.map(i -> ((Product) i.getValue())).collect(Collectors.toList()));
	}
	
	
	public static ArrayList<Worker> findWorkersForFactory(int idFactory){
		return new ArrayList<Worker>(workers.entrySet().stream()
				.filter(i -> ((Worker) i.getValue()).getFactory() == idFactory)
				.map(i -> ((Worker) i.getValue())).collect(Collectors.toList()));
	}
	
	
	public static ArrayList<Request> findRequestsForFactory(int idFactory){
		return new ArrayList<Request>(requests.entrySet().stream()
				.filter(i -> ((Request) i.getValue()).getFactory() == idFactory)
				.map(i -> ((Request) i.getValue())).collect(Collectors.toList()));
	}
	
	
	public static ArrayList<Factory> findFactoriesForCompany(int idCompany){
		return new ArrayList<Factory>(factories.entrySet().stream()
				.filter(i -> ((Factory) i.getValue()).getCompany() == idCompany)
				.map(i -> ((Factory) i.getValue())).collect(Collectors.toList()));
	}
	
	
	
	public static ArrayList<Outlet> findOutletsForCompany(int idCompany){
		return new ArrayList<Outlet>(outlets.entrySet().stream()
				.map(i -> ((Outlet) i.getValue())).collect(Collectors.toList()));
	}
	
	public static ArrayList<Employee> findEmployeesForCompany(int idCompany){
		
		ArrayList<Employee> companyEmployees = new ArrayList<Employee>();
		
		ArrayList<Worker> companyWorkers =  new ArrayList<Worker>(workers.entrySet().stream()
				.map(i -> ((Worker) i.getValue())).collect(Collectors.toList()));
		
		ArrayList<Seller> companySellers = new ArrayList<Seller>(sellers.entrySet().stream()
				.map(i -> ((Seller) i.getValue())).collect(Collectors.toList()));
		
		companyEmployees.addAll(companyWorkers);
		companyEmployees.addAll(companySellers);
		
		return companyEmployees;
	}
	
	public static ArrayList<Request> findRequestsForCompany(){
		return new ArrayList<Request>(requests.entrySet().stream()
				.filter(i -> ((Request) i.getValue()).getFactory() == -1)
				.map(i -> ((Request) i.getValue())).collect(Collectors.toList()));
	}
	
	public static ArrayList<Product> findProductsForCompany(int idCompany){
		return new ArrayList<Product>(products.entrySet().stream()
				.filter(i -> ((Product) i.getValue()).getCompany() == idCompany)
				.map(i -> ((Product) i.getValue())).collect(Collectors.toList()));
	}
	
	
	public static void dodaj(Identifiable object) throws Exception {
		
		//nisam fan ovih f-ja
		if (object instanceof Worker) {
			DAOinterface.dodaj(object, DAOinterface.workersPath);
		} else if (object instanceof Product) {
			DAOinterface.dodaj(object, DAOinterface.productsPath);
		} else if (object instanceof Seller) {
			DAOinterface.dodaj(object, DAOinterface.sellersPath);
		} else if (object instanceof Element) {
			DAOinterface.dodaj(object, DAOinterface.elementsPath);
		} else if (object instanceof Outlet) {
			DAOinterface.dodaj(object, DAOinterface.outletsPath);
		} else if (object instanceof Request) {
			DAOinterface.dodaj(object, DAOinterface.requestsPath);
		} else if (object instanceof Shipment) {
			DAOinterface.dodaj(object, DAOinterface.shipmentsPath);
		} else if (object instanceof Factory) {
			DAOinterface.dodaj(object, DAOinterface.factoriesPath);
		} 
	
	}
	
	public static void dodajLog(LogInterface object) throws Exception {
		if (object instanceof RequestLog) {
			DAOinterface.dodajLog(object, DAOinterface.requestLogPath);
		} else if (object instanceof ProductionLog) {
			DAOinterface.dodajLog(object, DAOinterface.productionLogPath);
		} else if (object instanceof TransportationLog) {
			DAOinterface.dodajLog(object, DAOinterface.transportLogPath);
		} else if (object instanceof SalesLog) {
			DAOinterface.dodajLog(object, DAOinterface.salesLogPath);
		}
	}
	
	public static void obrisi(Identifiable object) throws Exception {
		
		//nisam fan ovih f-ja
		if (object instanceof Worker) {
			DAOinterface.obrisi(object, DAOinterface.workersPath);
		} else if (object instanceof Product) {
			DAOinterface.obrisi(object, DAOinterface.productsPath);
		} else if (object instanceof Seller) {
			DAOinterface.obrisi(object, DAOinterface.sellersPath);
		} else if (object instanceof Element) {
			DAOinterface.obrisi(object, DAOinterface.elementsPath);
		} else if (object instanceof Outlet) {
			DAOinterface.obrisi(object, DAOinterface.outletsPath);
		} else if (object instanceof Request) {
			DAOinterface.obrisi(object, DAOinterface.requestsPath);
		} else if (object instanceof Shipment) {
			DAOinterface.obrisi(object, DAOinterface.shipmentsPath);
		} else if (object instanceof Factory) {
			DAOinterface.obrisi(object, DAOinterface.factoriesPath);
		}
	
	}
	
	
	public static void izmeni(Identifiable object) throws Exception {
		
		//nisam fan ovih f-ja
		if (object instanceof Worker) {
			DAOinterface.izmeni(object, DAOinterface.workersPath);
		} else if (object instanceof Product) {
			DAOinterface.izmeni(object, DAOinterface.productsPath);
		} else if (object instanceof Seller) {
			DAOinterface.izmeni(object, DAOinterface.sellersPath);
		} else if (object instanceof Element) {
			DAOinterface.izmeni(object, DAOinterface.elementsPath);
		} else if (object instanceof Outlet) {
			DAOinterface.izmeni(object, DAOinterface.outletsPath);
		} else if (object instanceof Request) {
			DAOinterface.izmeni(object, DAOinterface.requestsPath);
		} else if (object instanceof Shipment) {
			DAOinterface.izmeni(object, DAOinterface.shipmentsPath);
		} else if (object instanceof Factory) {
			DAOinterface.izmeni(object, DAOinterface.factoriesPath);
		} 
	
	}
	
	
	
	
	
	
	
	
} //od klase
