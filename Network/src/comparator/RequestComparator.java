package comparator;

import java.util.Comparator;

import model.Request;

//comparator which compares two Request object

public class RequestComparator implements Comparator<Request> {
	
	int direction = 1;
	
	public RequestComparator(int direction) {
		if(direction!=1 && direction!=-1){
			direction = 1;
		}
		this.direction = direction;
	}
	
	@Override
	public int compare(Request rq1, Request rq2) {
		
		if (rq1 == null || rq2 == null) {
			System.out.println("Ne postoje datumi za poredjenje!");
			System.exit(1);
		}
		
		if ( rq1.getDate().before(rq2.getDate())) {
            return -1*direction;
        } else if (rq1.getDate().after(rq2.getDate())) {
            return 1*direction;
        } else {
            return 0;
        } 
		
	}

}
