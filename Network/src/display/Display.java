package display;

import java.util.ArrayList;



import controller.DataStore;
import model.Company;
import model.Factory;
import model.Outlet;
import model.Product;
import model.Request;
import model.Role;
import model.Seller;
import model.Shipment;
import model.Worker;
import utility.Utility;

public class Display {
	
	//klase koja ima helper static metode koje ce se koristiti za 
	//prikaz i/ili izbor raznih elemenata, listi, objekata itd.
	
	
	public static void showCompaniesProducts(int idFactory) {
		
		Company company = (Company) DataStore.companies.get(idFactory);
		
		ArrayList<Product> products = company.getProducts();
		
		for (Product product : products) {
			System.out.println(product.display());
		}
	}
	
	
	public static Factory chooseFactory(Company company, String text) {
		
		System.out.println(text);
		
		ArrayList<Factory> factories = company.getFactories();
		
		for (Factory f : factories) {
			System.out.println(f.display());
		}
		
		while (true) {
			int idFactory = Utility.ocitajCeoBroj("Unesite ID fabrike: ");
			
			for (Factory f : factories) {
				if (f.getId() == idFactory) {
					return f;
				}
			}
			
			System.out.println("Unesite odgovarajuci ID fabrike");
			
		}
		
	}
	
	
	public static Outlet chooseOutlet(Company company, String text) {
		
		System.out.println(text);
		
		ArrayList<Outlet> outlets = company.getOutlets();
		
		for (Outlet o : outlets) {
			System.out.println(o.display());
		}
		while (true) {
			int idOutlet = Utility.ocitajCeoBroj("Unesite ID prodajnog mesta: ");
			
			for (Outlet o : outlets) {
				if (o.getId() == idOutlet) {
					return o;
				}
			}
			
			System.out.println("Unesite odgovarajuci ID prodajnog mesta.");
		}
		
	}
	
	public static void chooseProduct(ArrayList<Product> choosingList, ArrayList<Product> productsForFactory) {
		
		while(true) {
			int idProduct = Utility.ocitajCeoBroj("Unesite ID proizvoda: ");
			for(Product pr : choosingList) {
				if(pr.getId() == idProduct) {
					if (!productsForFactory.contains(pr)) {
						productsForFactory.add((Product) DataStore.products.get(idProduct));
						System.out.println("Uspe�no ste dodali proizvod u fabriku.");
						return;
					} else {
						System.out.println("Izabrani proizvod vec se nalazi u fabrici. Poku�ajte ponovo.");
						if (Utility.ocitajOdlukuOPotvrdi("prekinete akciju dodavanja proizvoda iz kompanijskog asortimana") == 'Y') {
							return;
						}
					}
				}
			}
			System.out.println("Unesite odgovarajuci ID proizvoda. Molimo Vas poku�ajte ponovo!");
		} //od while
	
	}
	
	
	public static void displayProductsFromOutlet(Outlet outlet) {
		
		for(Product product : outlet.getProducts()) {
			System.out.println(product.display());
		}
		
	}
	
	
	
	public static void factoriesOverview() {
		
		int idCompany = DataStore.companies.get(0).getId();
		Company company = (Company) DataStore.companies.get(idCompany);
		
		if (company.getFactories().size() == 0) {
			System.out.println("Trenutno kompanija nema nijednu fabriku.");
			return;
		}
		
		
		ArrayList<Factory> factories = company.getFactories();
		
		for (Factory f : factories) {
			System.out.println(f.display());
		}
		
		
	}
	
	public static void outletsOverview() {
		
		int idCompany = DataStore.companies.get(0).getId();
		Company company = (Company) DataStore.companies.get(idCompany);
		
		if (company.getOutlets().size() == 0) {
			System.out.println("Trenutno kompanija nema nijedno prodajno mesto.");
			return;
		}
		
		
		ArrayList<Outlet> outlets = company.getOutlets();
		
		for (Outlet o : outlets) {
			System.out.println(o.display());
		}
		
		
	}
	
	
	public static void displayRequestsForOutlet(ArrayList<Request> requestsList) {
		int counter = 1;
		for (Request rq : requestsList) {
			System.out.println("Redni br: " + counter++);
			System.out.println(rq.displayRequestForOutlet());
		}
		
	}
	
	public static void displayRequestsForCompany(ArrayList<Request> requestsList) {
		int counter = 1;
		for (Request rq : requestsList) {
			System.out.println("Redni br: " + counter++);
			System.out.println(rq.displayRequestForCompany());
		}
		
	}
	
	public static void displaySellersForOutlet(Outlet currentOutlet) {
		
		for (Seller sl : currentOutlet.getSellers()) {
			System.out.println(sl.display());
		}
		
	}
	
	
	public static int choosSellerFromOutlet(Outlet currentOutlet) {
		
		while(true) {
			int idSeller = Utility.ocitajCeoBroj("Unesite ID prodavca za kojeg zelite da procesuira ovaj zahtev: ");
			if (inSellersList(idSeller,currentOutlet.getSellers())) {
				return idSeller;
			} else {
				System.out.println("Unesite odgovarajucu vrednost.");
			}
		} //od while
		
	}
	
	private static boolean inSellersList(int idSeller, ArrayList<Seller> sellersList) {
		
		for (Seller s : sellersList) {
			if (s.getId() == idSeller) return true;
		}
		return false;
	}
	
	public static int chooseTransportSupervisorFromFactory(ArrayList<Worker> transporationSupervisors) {
		displayWorker(transporationSupervisors);
		while(true) {
			int idTransporter = Utility.ocitajCeoBroj("Unesite ID transportera kojeg zelite da dopremi ovu po�iljku: ");
			if (inList(idTransporter,transporationSupervisors)) {
				return idTransporter;
			} else {
				System.out.println("Unesite odgovarajucu vrednost.");
			}
		} //od while
		
	}
	
	private static boolean inList(int idTransporter,ArrayList<Worker> transporationSupervisors) {
		for (Worker w : transporationSupervisors) {
			if (w.getId() == idTransporter) return true;
		}
		return false;
	}
	
	
	public static int chooseProductionSupervisorFromFactory(ArrayList<Worker> productionSupervisors) {
		displayWorker(productionSupervisors);
		while(true) {
			int idSupervisor = Utility.ocitajCeoBroj("Unesite ID nadzornika kojeg zelite da nadgleda proizvodnju ove po�iljke: ");
			if (inList(idSupervisor,productionSupervisors)) {
				return idSupervisor;
			} else {
				System.out.println("Unesite odgovarajucu vrednost.");
			}
		} //od while
	}
	
	public static void displayWorker(ArrayList<Worker> workers) {
		for (Worker w : workers) {
			System.out.println(w.display());
		}
	}
	
	
	public static Shipment chooseShipment(Outlet outletForShipment) {
		
		ArrayList<Shipment> outletShipments = outletForShipment.getShipments();
		
		while (true) {
			int idRequest = Utility.ocitajCeoBroj("Unesite ID zahteva koji ste dobili prilikom kreiranja porudzbine: ");
			
			for (Shipment sh : outletShipments) {
				if (sh.getRequest() == idRequest) return sh;
			}
			
			System.out.println("Porudzbenica pod une�enim ID brojem ili jo� uvek nije dostavljena ili se nalazi pod drugim ID broje.");
			if (Utility.ocitajOdlukuOPotvrdi("da nastavite da prekinete akciju unosa ID porudzbenice") == 'Y') {
				return null;
			}
			
		}
		
		
	}
	
	public static boolean haveRequiredWorkers(Factory currentFactory) {
		int types = 3; //three worker types required: worker, supervisor, transporter
		int currentRoles = 0;
		for (Worker w : currentFactory.getWorkers()) {
			Role workerRole = w.getRole();
			if (workerRole == Role.PRODUCTION_WORKER || workerRole == Role.PRODUCTION_SUPERVISOR || workerRole == Role.TRANSPORT_SUPERVISOR) {
				currentRoles++;
				if (currentRoles == types) return false;
			}
		}
		return true;
	}

} //od klase
