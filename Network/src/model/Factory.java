package model;

import java.util.ArrayList;
import java.util.StringJoiner;

import controller.DataStore;




public class Factory extends Identifiable{
	
	private String name;
	private int company;
	private ArrayList<Product> products;
	private ArrayList<Worker> workers;
	private ArrayList<Request> requests;
	
	
	public Factory(int id, int company, ArrayList<Product> products, ArrayList<Worker> workers,
			String name,  ArrayList<Request> requests) {
		
		super(id);
		this.company = company;
		this.products = products;
		this.workers = workers;
		this.name = name;
		this.requests = requests;
		
	}
	
	public Factory() {
		this(-1, -1, null, null, "", null);
	}

	public int getCompany() {
		return company;
	}

	public void setCompany(int company) {
		this.company = company;
	}

	public ArrayList<Product> getProducts() {
		return products;
	}

	public void setProducts(ArrayList<Product> products) {
		this.products = products;
	}

	public ArrayList<Worker> getWorkers() {
		return workers;
	}

	public void setWorkers(ArrayList<Worker> workers) {
		this.workers = workers;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ArrayList<Request> getRequests() {
		return requests;
	}

	public void setRequests(ArrayList<Request> requests) {
		this.requests = requests;
	}

	
	
	public static Identifiable CreateFromString(String text) {
		
		var factory = new Factory();
		
		String[] factoryParts = text.trim().split("\\|");
		
		factory.setId(Integer.valueOf(factoryParts[0]));
		factory.setName(factoryParts[1]);
		factory.setCompany(Integer.valueOf(factoryParts[2]));
		factory.setProducts(DataStore.findProductsForFactory(factory.getId()));
		factory.setWorkers(DataStore.findWorkersForFactory(factory.getId()));
		factory.setRequests(DataStore.findRequestsForFactory(factory.getId()));
		
		return factory;
	}
	
	@Override
	public String WriteToString() {
		var line = new StringJoiner("|");
		
		line.add(this.getId()+"").add(this.getName()).add(this.getCompany()+"");
		return line.toString();
	}
	
	public String display() {
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("ID fabrike: " + this.getId()+"\n");
		sb.append("\tNaziv: " + this.getName()+"\n");
		sb.append("\tLista proizvoda: " + displayProducts()+"\n");
		sb.append("\tKompanija: " + ((Company)DataStore.companies.get(this.getCompany())).getName()+"\n");
	
	
		return sb.toString();
	}
	
	private String displayProducts() {
		
		StringBuilder sb = new StringBuilder();
		
		for (Product pr : this.products) {
			sb.append(pr.getName() + " ");
		}
		
		return sb.toString();
	}
}
