package model;

import java.util.StringJoiner;

public class Worker extends Employee {
	
	private int factory;

	public Worker(int id, Role role, String name, double salary, String mobile, String username, String password, int factory) {
		
		super(id, role, name, salary, mobile, username, password);
		this.factory = factory;
	}
	
	
	public Worker() {
		this(-1, Role.UNSPECIFIED, "", 0, "", "", "", -1);
	}


	public int getFactory() {
		return factory;
	}


	public void setFactory(int factory) {
		this.factory = factory;
	}
	
	
	public static Identifiable CreateFromString(String text) {
		
		var worker = new Worker();
		
		String[] workerParts = text.trim().split("\\|");
		
		worker.setId(Integer.valueOf(workerParts[0]));
		worker.setRole(Role.getRole(Integer.valueOf(workerParts[1])));
		worker.setName(workerParts[2]);
		worker.setSalary(Double.valueOf(workerParts[3]));
		worker.setMobile(workerParts[4]);
		worker.setUsername(workerParts[5]);
		worker.setPassword(workerParts[6]);
		worker.setFactory(Integer.valueOf(workerParts[7]));
		
		return worker;
	}
	
	@Override
	public String WriteToString() {
		var line = new StringJoiner("|");
		line.add(super.WriteToString());
		line.add(this.getFactory()+"");
		return line.toString();
	}
	
	public String display() {
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("ID radnika: " + this.getId()+"\n");
		sb.append("\tIme: " + this.getName()+"\n");
		sb.append("\tUloga: " + this.getRole().getText()+"\n");
		
		return sb.toString();
		
	}
	
	
	
	
}
