package model;

import java.util.ArrayList;
import java.util.Date;

import DAO.LogInterface;
import controller.DataStore;
import utility.Utility;

public class RequestLog implements LogInterface{
	
	
	private Request request;
	
	
	public RequestLog(Request request) {
		this.request = request;
	}


	@Override
	public String writeLogToFile() {
		StringBuilder sb = new StringBuilder();
		
		String outlet = findOutlet();
		
		String list = findProductsWithAmounts();
		
		String date = Utility.convertDateWithTimeToString(new Date());
		
		return sb.append("[" + date + "]" + " [Zahtev za isporuku] " + "[Prodajno mesto: " + outlet +"]"  + " [Proizvodi: " + list +"]"+"\n").toString();
	}
	
	private String findProductsWithAmounts() {
		ArrayList<Element> elements = this.request.getElements();
		StringBuilder sb = new StringBuilder();
		for (Element el : elements) {
			String product = ((Product) DataStore.products.get(el.getProduct())).getName();
			String amount = String.valueOf(el.getAmount());
			sb.append(product + " x " + amount+",");
		}
		
		return sb.toString();
	}
	
	private String findOutlet() {
		Outlet outlet = (Outlet) DataStore.outlets.get(request.getOutlet());
		return outlet.getName();
	}
	
	
}
