package model;

import java.util.StringJoiner;

import controller.DataStore;

public class Seller extends Employee {
	
	private int outlet;

	public Seller(int id, Role role, String name, double salary, String mobile, String username, String password, int outlet) {
		
		super(id, role, name, salary, mobile, username, password);
		this.outlet = outlet;
	}
	
	public Seller() {
		this(-1, Role.UNSPECIFIED, "", 0, "", "", "", -1);
	}

	public int getOutlet() {
		return outlet;
	}

	public void setOutlet(int outlet) {
		this.outlet = outlet;
	}
	
	public static Identifiable CreateFromString(String text) {
		
		var seller = new Seller();
		
		String[] sellerParts = text.trim().split("\\|");
		
		seller.setId(Integer.valueOf(sellerParts[0]));
		seller.setRole(Role.getRole(Integer.valueOf(sellerParts[1])));
		seller.setName(sellerParts[2]);
		seller.setSalary(Double.valueOf(sellerParts[3]));
		seller.setMobile(sellerParts[4]);
		seller.setUsername(sellerParts[5]);
		seller.setPassword(sellerParts[6]);
		seller.setOutlet(Integer.valueOf(sellerParts[7]));
		
		return seller;
	}
	
	@Override
	public String WriteToString() {
		var line = new StringJoiner("|");
		line.add(super.WriteToString());
		line.add(this.getOutlet()+"");
		return line.toString();
	}
	
	
	public String display() {
		StringBuilder sb = new StringBuilder();
		
		sb.append("ID prodavca: " + this.getId()+"\n");
		sb.append("\tIme: " + this.getName()+"\n");
		sb.append("\tMobilni: " + this.getMobile()+"\n");
		sb.append("\tProdajno mesto: " + ((Outlet)DataStore.outlets.get(this.getOutlet())).getName()+"\n");
		
		return sb.toString();
		
		
	}
	
	
	
	
}
