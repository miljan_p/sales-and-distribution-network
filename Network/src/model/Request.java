package model;

import java.util.ArrayList;
import java.util.Date;
import java.util.StringJoiner;

import controller.DataStore;
import utility.Utility;

public class Request extends Identifiable {
	
	private int outlet;
	private ArrayList<Element> elements;
	private Date date;
	private int seller;
	private int factory;

	
	
	public Request(int id, int outlet, ArrayList<Element> elements, Date date, int seller, int factory) {
		super(id);
		this.outlet = outlet;
		this.elements = elements;
		this.date = date;
		this.seller = seller;
		this.factory = factory;
		
	}
	
	public Request() {
		this(-1, -1, null, null, -1, -1);
	}

	
	
	public int getOutlet() {
		return outlet;
	}

	public void setOutlet(int outlet) {
		this.outlet = outlet;
	}

	public ArrayList<Element> getElements() {
		return elements;
	}

	public void setElements(ArrayList<Element> elements) {
		this.elements = elements;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getSeller() {
		return seller;
	}

	public void setSeller(int seller) {
		this.seller = seller;
	}

	public int getFactory() {
		return factory;
	}

	public void setFactory(int factory) {
		this.factory = factory;
	}
	
	

	public static Identifiable CreateFromString(String text) {
		
		var request = new Request();
		
		String[] requestParts = text.trim().split("\\|");
		
		request.setId(Integer.valueOf(requestParts[0]));
		request.setOutlet(Integer.valueOf(requestParts[1]));
		request.setDate(Utility.createDateWithTime(requestParts[2]));
		request.setSeller(Integer.valueOf(requestParts[3]));
		request.setFactory(Integer.valueOf(requestParts[4]));
		request.setElements(DataStore.findElementsForRequest(request.getId()));
		
		
		return request;
	}
	
	@Override
	public String WriteToString() {
		var line = new StringJoiner("|");
		
		line.add(this.getId()+"").add(this.getOutlet()+"").add(Utility.convertDateWithTimeToString(this.getDate()))
		.add(this.getSeller()+"").add(this.getFactory()+"");
		return line.toString();
	}
	
	public String displayRequestForOutlet() {
		StringBuilder sb = new StringBuilder();
		
		sb.append("ID zahteva: " + this.getId()+"\n");
		sb.append("\tDatum zahteva: " + Utility.convertDateWithTimeToString(this.getDate())+"\n");
		sb.append("\tPoruceni proizvodi: " + orderedProducts()+"\n");
		
		
		return sb.toString();
		
	}
	
	public String displayRequestForCompany() {
		StringBuilder sb = new StringBuilder();
		
		sb.append("ID zahteva: " + this.getId()+"\n");
		sb.append("\tDatum zahteva: " + Utility.convertDateWithTimeToString(this.getDate())+"\n");
		sb.append("\tPoruceni proizvodi: " + orderedProducts()+"\n");
		sb.append("\tProdajno mesto: " + ((Outlet)DataStore.outlets.get(this.getOutlet())).getName()+"\n");
		
		return sb.toString();
		
	}
	
	
	private String orderedProducts() {
		StringBuilder sb = new StringBuilder();
		for (Element el : this.getElements()) {
			String product = ((Product)DataStore.products.get(el.getProduct())).getName();
			int amount = el.getAmount();
			sb.append(product + " x " + amount + ",");
		}
		
		return sb.toString();
	}
	
}
