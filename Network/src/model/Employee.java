package model;

import java.util.StringJoiner;

public abstract class Employee extends Identifiable {
	
	protected Role role;
	protected String name;
	protected double salary;
	protected String mobile;
	protected String username;
	protected String password;
	
	
	public Employee(int id, Role role, String name, double salary, String mobile, String username, String password) {
		super(id);
		this.role = role;
		this.name = name;
		this.salary = salary;
		this.mobile = mobile;
		this.username = username;
		this.password = password;
	}
	
	
	public Employee() {
		this(-1, null, "", 0, "", "", "");
	}


	public Role getRole() {
		return role;
	}


	public void setRole(Role role) {
		this.role = role;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public double getSalary() {
		return salary;
	}


	public void setSalary(double salary) {
		this.salary = salary;
	}


	public String getMobile() {
		return mobile;
	}


	public void setMobile(String mobile) {
		this.mobile = mobile;
	}


	public String getUsername() {
		return username;
	}


	public void setUsername(String username) {
		this.username = username;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}
	
	@Override
	public String WriteToString() {
		var line = new StringJoiner("|");
		
		line.add(this.getId()+"").add(this.getRole().toFileString()).add(this.getName()).add(this.getSalary()+"")
		.add(this.getMobile()).add(this.getUsername()).add(this.getPassword());
		
		return line.toString();
	}
	
	
}
