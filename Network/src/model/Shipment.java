package model;

import java.util.ArrayList;
import java.util.StringJoiner;

import controller.DataStore;




public class Shipment extends Identifiable {
	
	private int request;
	private int outlet;
	private int productionSupervisor;
	private int transportationSupervisor;	//transport
	private int factory;
	private ArrayList<Product> products;
	
	
	public Shipment(int id, int request, int outlet, int productionSupervisor, int transportationSupervisor, int factory, ArrayList<Product> products) {
		super(id);
		this.request = request;
		this.outlet = outlet;
		this.productionSupervisor = productionSupervisor;
		this.transportationSupervisor = transportationSupervisor;
		this.factory = factory;
		this.products = products;
	}
	
	
	public Shipment() {
		this(-1, -1, -1, -1, -1, -1, null);
	}


	
	
	public int getRequest() {
		return request;
	}


	public void setRequest(int request) {
		this.request = request;
	}


	public int getOutlet() {
		return outlet;
	}


	public void setOutlet(int outlet) {
		this.outlet = outlet;
	}


	public int getProductionSupervisor() {
		return productionSupervisor;
	}


	public void setProductionSupervisor(int productionSupervisor) {
		this.productionSupervisor = productionSupervisor;
	}


	public int getTransportationSupervisor() {
		return transportationSupervisor;
	}


	public void setTransportationSupervisor(int transportationSupervisor) {
		this.transportationSupervisor = transportationSupervisor;
	}


	public int getFactory() {
		return factory;
	}


	public void setFactory(int factory) {
		this.factory = factory;
	}


	public ArrayList<Product> getProducts() {
		return products;
	}


	public void setProducts(ArrayList<Product> products) {
		this.products = products;
	}


	public static Identifiable CreateFromString(String text) {
		
		var shipment = new Shipment();
		
		String[] shipmentParts = text.trim().split("\\|");
		
		shipment.setId(Integer.valueOf(shipmentParts[0]));
		shipment.setRequest(Integer.valueOf(shipmentParts[1]));
		shipment.setOutlet(Integer.valueOf(shipmentParts[2]));
		shipment.setProductionSupervisor(Integer.valueOf(shipmentParts[3]));
		shipment.setTransportationSupervisor(Integer.valueOf(shipmentParts[4]));
		shipment.setFactory(Integer.valueOf(shipmentParts[5]));
		shipment.setProducts(DataStore.findProductsForShipment(shipment.getId())); //proseledi idShipment-a i preko java streama napravi arraylistu producta iz fajla shipmentproduct
		
		return shipment;
	}
	
	@Override
	public String WriteToString() {
		var line = new StringJoiner("|");
		
		line.add(this.getId()+"").add(this.getRequest()+"")
		.add(this.getOutlet()+"").add(this.getProductionSupervisor()+"").add(this.getTransportationSupervisor()+"").add(this.getFactory()+"");
		return line.toString();
	}

}
