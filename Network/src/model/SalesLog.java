package model;

import java.util.Date;

import DAO.LogInterface;
import controller.DataStore;
import utility.Utility;

public class SalesLog implements LogInterface{
	
	private Shipment shipment;
	private String name;
	private String meansOfPaying;
	
	public SalesLog(Shipment shipment, String name, String meansOfPaying) {
		this.shipment = shipment;
		this.name = name;
		this.meansOfPaying = meansOfPaying;
	}
	
	
	@Override
	public String writeLogToFile() {
		StringBuilder sb = new StringBuilder();
		
		String serialNumbesList = findSerialNumbers();
		
		String outlet = findOutlet();
		
		String seller = findSeller();
		
		String date = Utility.convertDateWithTimeToString(new Date());
		
		return sb.append("[" + date + "]" + " [Placanje proizvoda] " + "[Lista serijskih brojeva proizvoda: " + serialNumbesList +"] " + "[Prodajno mesto: " + outlet + "] " + "[Prodavac: " + seller + "] " + "[Nacin placanja: " + this.meansOfPaying + "]" + "[Ime kupca: " + this.name + "]" +"\n").toString();

		
	}
	
	private String findSerialNumbers() {
		StringBuilder sb = new StringBuilder();
		
		for (Product pr : this.shipment.getProducts()) {
			sb.append(" " + pr.getSerialNumber()+ " ");
		}
		return sb.toString();
	}
	
	private String findOutlet() {
		
		
		int idOutlet = this.shipment.getOutlet();
				
		Outlet outlet =	(Outlet) DataStore.outlets.get(idOutlet);
		
		return outlet.getName();
			
					
	}
	
	private String findSeller() {
		
		int idRequest = this.shipment.getRequest();
		
		Request request = (Request) DataStore.requests.get(idRequest);
		
		int idSeller = request.getSeller();
		
		Seller seller = (Seller) DataStore.sellers.get(idSeller);
		
		return seller.getName();
				
	}
	
	
}
