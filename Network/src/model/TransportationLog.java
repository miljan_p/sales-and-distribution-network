package model;

import java.util.ArrayList;
import java.util.Date;

import DAO.LogInterface;
import controller.DataStore;
import utility.Utility;

public class TransportationLog implements LogInterface{
	
	private Shipment shipment;
	
	public TransportationLog(Shipment shipment) {
		this.shipment = shipment;
	}

	@Override
	public String writeLogToFile() {
		
		StringBuilder sb = new StringBuilder();
		
		String factory = ((Factory)DataStore.factories.get(this.shipment.getFactory())).getName();
		String outlet = ((Outlet) DataStore.outlets.get(this.shipment.getOutlet())).getName();
		
		String listOfProducts = findProductsWithAmounts(this.shipment.getRequest());
		
		String transportSupervisor = ((Worker)DataStore.workers.get(this.shipment.getTransportationSupervisor())).getName();
		
		String date = Utility.convertDateWithTimeToString(new Date());
		
		return sb.append("[" + date + "]" + " [Transport proizvoda] " + "[Proizvodi: " + listOfProducts +"] " + "[Iz: " + factory + ", u: " + outlet + "] " + "[Nadzornik transporta: " + transportSupervisor + "]"  +"\n").toString();
	}
	
	private String findProductsWithAmounts(int idRequest) {
		
		Request request = (Request) DataStore.requests.get(idRequest);
		
		ArrayList<Element> elements = request.getElements();
		StringBuilder sb = new StringBuilder();
		for (Element el : elements) {
			String product = ((Product) DataStore.products.get(el.getProduct())).getName();
			String amount = String.valueOf(el.getAmount());
			sb.append(product + " x " + amount+",");
		}
		
		return sb.toString();
	}

	public Shipment getShipment() {
		return shipment;
	}

	
	

}
