package model;

import java.util.Date;

import DAO.LogInterface;
import controller.DataStore;
import utility.Utility;

public class ProductionLog implements LogInterface{
	
	private Product product;
	private int idProductionSupervisor;
	
	public ProductionLog(Product product, int idProductionSupervisor) {
		this.product = product;
		this.idProductionSupervisor = idProductionSupervisor;
	}

	@Override
	public String writeLogToFile() {
		
		StringBuilder sb = new StringBuilder();
		String productName = product.getName();
		String serialNumber = String.valueOf(product.getSerialNumber());
		String factoryName = ((Factory) DataStore.factories.get(this.product.getFactory())).getName();
		String productionSupervisor = ((Worker)DataStore.workers.get(idProductionSupervisor)).getName();
				
		String date = Utility.convertDateWithTimeToString(new Date());
		
		return sb.append("[" + date + "]" + " [Proizvodnja proizvoda] " + "[Proizvod: " + productName +"]"  + " [Serijski broj: " + serialNumber +"] "+ "[Fabrika: " + factoryName +"]" + " [Nadzornik proizvodnje: " + productionSupervisor +"]"+"\n").toString();
	}
	
	
	
	
}
