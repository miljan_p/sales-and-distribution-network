package model;

import java.util.StringJoiner;
import java.util.function.Function;

import DAO.DAOinterface;
import controller.DataStore;


public class Product extends Identifiable {
	
	protected int company;
	protected int factory;
	protected String name;
	protected double price;
	protected int serialNumber;
	
	
	public Product(int id, int company, int factory, String name, double price) {
		
		super(id);
		this.company = company;
		this.factory = factory;
		this.name = name;
		this.price = price;
		this.serialNumber = generateSerialNumber(); //generateSerial
		
		
	}
	
	public Product(Product other) {
		this.id = other.getId();
		this.company = other.getCompany();
		this.factory = other.getFactory();
		this.name = other.getName();
		this.price = other.getPrice();
		this.serialNumber = generateSerialNumber();
	}
	
	public Product(int id, int company, int factory, String name, double price, int serialNumber) {
		
		super(id);
		this.factory = factory;
		this.name = name;
		this.price = price;
		this.serialNumber = serialNumber; 
		
		
	}
	
	public Product() {
		this(-1, -1, -1, "", -1, -1);
	}
	
	private int generateSerialNumber() {
		
		return DAOinterface.generateSerial();
	}
	
	
	public int getCompany() {
		return company;
	}

	public void setCompany(int company) {
		this.company = company;
	}

	public int getFactory() {
		return factory;
	}

	public void setFactory(int factory) {
		this.factory = factory;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(int serialNumber) {
		this.serialNumber = serialNumber;
	}

	
	public static Identifiable CreateFromString(String text) {
		
		var product = new Product();
		
		String[] productParts = text.trim().split("\\|");
		
		product.setId(Integer.valueOf(productParts[0]));
		product.setCompany(Integer.valueOf(productParts[1]));
		product.setFactory(Integer.valueOf(productParts[2]));
		product.setName(productParts[3]);
		product.setPrice(Double.valueOf(productParts[4]));
		
		
		return product;
		
		
	}
	
	public static Function<String, Product> mapLineToProduct = new Function<String, Product>() {

	    public Product apply(String line) {

	    	Product product = new Product();

	    	String[] productParts = line.trim().split("\\|");
			
			product.setId(Integer.valueOf(productParts[1]));
			product.setFactory(Integer.valueOf(productParts[2]));
			product.setName(productParts[3]);
			product.setPrice(Double.valueOf(productParts[4]));
	        product.setSerialNumber(Integer.valueOf(productParts[5]));
	        
	        return product;
	    }
	};
	
	@Override
	public String WriteToString() {
		var line = new StringJoiner("|");
		
		line.add(this.getId()+"").add(this.getCompany()+"").add(this.getFactory()+"").add(this.getName()).add(this.getPrice()+"");
		return line.toString();
	}
	
	public String writeProductToShipment(int idShipment) {
		var line = new StringJoiner("|");
		
		line.add(idShipment+"").add(this.getId()+"").add(this.getCompany()+"").add(this.getFactory()+"").add(this.getName()).add(this.getPrice()+"").add(this.getSerialNumber()+"");
		return line.toString();
	}
	
	public String display() {
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("ID proizvoda: " + this.getId()+"\n");
		sb.append("\tNaziv: " + this.getName()+"\n");
		sb.append("\tCena: " + this.getPrice()+"\n");
		sb.append("\tKompanija: " + ((Company)DataStore.companies.get(this.getCompany())).getName()+"\n");
		
		return sb.toString();
		
	}
}
