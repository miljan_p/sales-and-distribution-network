package model;

public enum Role {
	
	SELLER,
	PRODUCTION_WORKER,
	TRANSPORT_WORKER,
	PRODUCTION_SUPERVISOR,
	TRANSPORT_SUPERVISOR,
	UNSPECIFIED;
	
	private String text;
	
	static {
		SELLER.text = "Seller";
		PRODUCTION_WORKER.text = "Manufactor Worker";
		TRANSPORT_WORKER.text = "Transportation Worker";
		PRODUCTION_SUPERVISOR.text = "Production Supervisor";
		TRANSPORT_SUPERVISOR.text = "Transportation Supervisor";
		
	
	}

	public String getText() {
		return text;
	}
	
	public static Role getRole(int ord) {
		switch(ord) {
		case 0: return Role.SELLER;
		case 1: return Role.PRODUCTION_WORKER;
		case 2: return Role.TRANSPORT_WORKER;
		case 3: return Role.PRODUCTION_SUPERVISOR;
		case 4: return Role.TRANSPORT_SUPERVISOR;
		default:return Role.UNSPECIFIED;
		}
	}
	
	public int roleToInteger() {
		if (this.text.equals("Seller")) {
			return 0;
		} else if (this.text.equals("Manufactor Worker")){
			return 1;
		} else if (this.text.equals("Transportation Worker")){
			return 2;
		} else if (this.text.equals("Production Supervisor")){
			return 3;
		} else if (this.text.equals("Transportation Supervisor")){
			return 4;
		} else return -1;
		
	}
	
	public String toFileString() {
		if (this.text.equals("Seller")) {
			return "0";
		} else if (this.text.equals("Manufactor Worker")){
			return "1";
		} else if (this.text.equals("Transportation Worker")){
			return "2";
		} else if (this.text.equals("Production Supervisor")){
			return "3";
		} else if (this.text.equals("Transportation Supervisor")){
			return "4";
		} else return "-1";
	}

}
