package model;

import java.util.StringJoiner;

public class Element extends Identifiable {
	
	private int request;
	private int product;
	private int amount;
	
	public Element(int id, int request,int product, int amount) {
		super(id);
		this.product = product;
		this.amount = amount;
		this.request = request;
	}
	
	
	public Element() {
		this(-1, -1, -1, -1);
	}


	public int getProduct() {
		return product;
	}


	public void setProduct(int product) {
		this.product = product;
	}


	public int getRequest() {
		return request;
	}


	public void setRequest(int request) {
		this.request = request;
	}


	public int getAmount() {
		return amount;
	}


	public void setAmount(int amount) {
		this.amount = amount;
	}
	
	
	public static Identifiable CreateFromString(String text) {
		
		var element = new Element();
		
		String[] elementParts = text.trim().split("\\|");
		
		element.setId(Integer.valueOf(elementParts[0]));
		element.setRequest(Integer.valueOf(elementParts[1]));
		element.setProduct(Integer.valueOf(elementParts[2]));
		element.setAmount(Integer.valueOf(elementParts[3]));
		
		return element;
	}
	
	@Override
	public String WriteToString() {
		var line = new StringJoiner("|");
		
		line.add(this.getId()+"").add(this.getRequest()+"").add(this.getProduct()+"").add(this.getAmount()+"");
		return line.toString();
	}
	

}
