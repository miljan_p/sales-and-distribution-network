package model;

import java.util.ArrayList;
import java.util.StringJoiner;

import controller.DataStore;

public class Company extends Identifiable{
	
	private static Company uniqueInstance;
	private ArrayList<Factory> factories;
	private ArrayList<Outlet> outlets;
	private ArrayList<Employee> employees;
	private String name;
	private ArrayList<Request> requests;
	private ArrayList<Product> products;
	
	

	private Company(int id, ArrayList<Factory> factories, ArrayList<Outlet> outlets, ArrayList<Employee> employees,
			String name, ArrayList<Request> requests, ArrayList<Product> products) {
		super(id);
		this.factories = factories;
		this.outlets = outlets;
		this.employees = employees;
		this.name = name;
		this.requests = requests;
		this.products = products;
	}


	private Company(){
		this(-1, null, null, null, "", null, null);
	}
	
	
	public static Company getInstance() {
		if (uniqueInstance == null) {
			uniqueInstance = new Company();
			uniqueInstance.setId(0);
			uniqueInstance.setName("The Shelby Limited");
			uniqueInstance.setFactories(DataStore.findFactoriesForCompany(uniqueInstance.getId()));
			uniqueInstance.setOutlets(DataStore.findOutletsForCompany(uniqueInstance.getId()));
			uniqueInstance.setEmployees(DataStore.findEmployeesForCompany(uniqueInstance.getId()));
			uniqueInstance.setRequests(DataStore.findRequestsForCompany());
			uniqueInstance.setProducts(DataStore.findProductsForCompany(uniqueInstance.getId()));
		}
		return uniqueInstance;
	}

	public ArrayList<Factory> getFactories() {
		return factories;
	}


	public void setFactories(ArrayList<Factory> factories) {
		this.factories = factories;
	}


	public ArrayList<Outlet> getOutlets() {
		return outlets;
	}


	public void setOutlets(ArrayList<Outlet> outlets) {
		this.outlets = outlets;
	}


	public ArrayList<Employee> getEmployees() {
		return employees;
	}


	public void setEmployees(ArrayList<Employee> employees) {
		this.employees = employees;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public ArrayList<Request> getRequests() {
		return requests;
	}


	public void setRequests(ArrayList<Request> requests) {
		this.requests = requests;
	}
	
	
	public ArrayList<Product> getProducts() {
		return products;
	}


	public void setProducts(ArrayList<Product> products) {
		this.products = products;
	}


	public static Identifiable CreateFromString(String text) {
		
		var company = new Company();
		
		String[] companyParts = text.trim().split("\\|");
		
		company.setId(Integer.valueOf(companyParts[0]));
		company.setName(companyParts[1]);
		company.setFactories(DataStore.findFactoriesForCompany(company.getId()));
		company.setOutlets(DataStore.findOutletsForCompany(company.getId()));
		company.setEmployees(DataStore.findEmployeesForCompany(company.getId()));
		company.setRequests(DataStore.findRequestsForCompany());
		
		return company;
	}
	
	
	@Override
	public String WriteToString() {
		var line = new StringJoiner("|");
		
		line.add(this.getId()+"").add(this.getName());
		return line.toString();
	}
	

}
