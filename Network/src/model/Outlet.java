package model;

import java.util.ArrayList;
import java.util.StringJoiner;

import controller.DataStore;

public class Outlet extends Identifiable {
	
	private String name;
	private int company;
	private ArrayList<Product> products;
	private ArrayList<Seller> sellers;
	private ArrayList<Shipment> shipments; //gotove isporuke; tj gotovi zahtevi
	private ArrayList<Request> requests; 
	
	
	public Outlet(int id, int company, ArrayList<Product> products, ArrayList<Seller> sellers,  String name, ArrayList<Shipment> shipments, ArrayList<Request> requests) {
		super(id);
		this.company = company;
		this.products = products;
		this.sellers = sellers;
		this.name = name;
		this.shipments = shipments;
		this.requests = requests;
		
	}
	
	public Outlet() {
		this(-1, -1, null, null, "", null, null);
	}
	
	public int getCompany() {
		return company;
	}

	public void setCompany(int company) {
		this.company = company;
	}

	public ArrayList<Product> getProducts() {
		return products;
	}

	public void setProducts(ArrayList<Product> products) {
		this.products = products;
	}

	public ArrayList<Seller> getSellers() {
		return sellers;
	}

	public void setSellers(ArrayList<Seller> sellers) {
		this.sellers = sellers;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ArrayList<Shipment> getShipments() {
		return shipments;
	}

	public void setShipments(ArrayList<Shipment> shipments) {
		this.shipments = shipments;
	}
	
	public ArrayList<Request> getRequests() {
		return this.requests;
	}

	public void setRequests(ArrayList<Request> requests) {
		this.requests = requests;
	}

	public static Identifiable CreateFromString(String text) {
		
		var outlet = new Outlet();
		
		String[] outletParts = text.trim().split("\\|");
		
		outlet.setId(Integer.valueOf(outletParts[0]));
		outlet.setName(outletParts[1]);
		outlet.setCompany(Integer.valueOf(outletParts[2]));
		outlet.setProducts(DataStore.findProductsForOutlet());
		outlet.setSellers(DataStore.findSellersForOutlet(outlet.getId()));
		outlet.setShipments(DataStore.findShipmentsForOutlet(outlet.getId()));
		outlet.setRequests(DataStore.findRequestsForOutlet(outlet.getId()));
		
		return outlet;
	}
	
	@Override
	public String WriteToString() {
		var line = new StringJoiner("|");
		
		line.add(this.getId()+"").add(this.getName()).add(this.getCompany()+"");
		return line.toString();
	}
	
	public String display() {
		StringBuilder sb = new StringBuilder();
		
		sb.append("ID prodajnog mesta: " + this.getId()+"\n");
		sb.append("\t\tNaziv: " + this.getName()+"\n");
		sb.append("\t\tLista proizvoda: " + displayProducts()+"\n");
		sb.append("\t\tZapolseni: " + displaySellers()+"\n");
		sb.append("\t\tKompanija: " + ((Company)DataStore.companies.get(this.getCompany())).getName()+"\n");
	
		return sb.toString();
	}
	
	private String displayProducts() {
		StringBuilder sb = new StringBuilder();
		
		for (Product pr : this.products) {
			sb.append(pr.getName() + " ");
		}
		
		return sb.toString();
	}
	
	private String displaySellers() {
		StringBuilder sb = new StringBuilder();
		
		for (Seller sl : this.sellers) {
			sb.append(sl.getName() + ",");
		}
		
		return sb.toString();
	}
	
	
	
	
}
